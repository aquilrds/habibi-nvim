## Habibi Nvim

### Description
This is a custom nvim configuration refered from here [here](https://www.youtube.com/watch?v=vdn_pKJUda8&t=554s)

### Prerequisite
- Neovim installed (>0.8.0)

### Steps to install

- Run `git clone git@gitlab.com:aquilrds/habibi-nvim.git ~/.config/nvim/`
- Run `nvim .` for nvim to open
- Run `:PackerSync` to install all the required packages
- Run `:Mason` to install all the lsp dependencies
