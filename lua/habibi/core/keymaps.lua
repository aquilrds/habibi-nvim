vim.g.mapleader = " "

local keymap = vim.keymap

--general keymaps

keymap.set("i", "jj", "<ESC>")
keymap.set("n", "<leader>nh", ":nohl<CR>")
keymap.set("n", "x", '"_x')

keymap.set("n", "<leader>sl", "<C-w>v")
keymap.set("n", "<leader>sj", "<C-w>s")
keymap.set("n", "<leader>se", "<C-w>=")
keymap.set("n", "<leader>sc", ":close<CR>")
keymap.set("n", "<leader>sll", "<C-w>v:term<CR>")
keymap.set("n", "<leader>sjj", "<C-w>s:term<CR>")

keymap.set("n", "<leader>to", ":tabnew<CR>") --open tab
keymap.set("n", "<leader>tc", ":tabclose<CR>") --close tab
keymap.set("n", "<leader>tl", ":tabn<CR>") -- got to nex tab
keymap.set("n", "<leader>th", ":tabp<CR>") -- go to previous tab

keymap.set("t", "jj", [[<C-\><C-n>]]) -- escase terminal
--plugin keymaps

-- vim maximizer
keymap.set("n", "<leader>sm", ":MaximizerToggle<CR>")

-- nvim-tree keymap
keymap.set("n", "<leader>e", ":NvimTreeToggle<CR>")

--telescope
keymap.set("n", "<leader>ff", "<cmd>Telescope find_files<cr>")
keymap.set("n", "<leader>fw", "<cmd>Telescope live_grep<cr>")
keymap.set("n", "<leader>fc", "<cmd>Telescope grep_string<cr>")
keymap.set("n", "<leader>fb", "<cmd>Telescope buffers<cr>")
keymap.set("n", "<leader>fh", "<cmd>Telescope help_tags<cr>")
