-- local mason_setup, mason = pcall(require, "mason")
-- if not mason_status then
--   print("mason not found")
--   return
-- end
--
local mason_null_ls_status, mason_null_ls = pcall(require, "mason-null-ls")
if not mason_null_ls_status then
	print("no mason nuls")
	return
end

-- mason.setup()
require("mason").setup()

require("mason-lspconfig").setup({
	automatic_installation = true,
	ensure_installed = { "html", "pyright", "lua_ls", "clangd" },
})

mason_null_ls.setup({
	ensure_installed = {
		"prettier",
		"stylua",
		"black",
		"flake8",
	},
})
