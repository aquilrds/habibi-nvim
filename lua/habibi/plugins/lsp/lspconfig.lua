local lspconfig_status, lspconfig = pcall(require, "lspconfig")
if not lspconfig_status then
	print("lspconfig not found")
	return
end

local cmp_nvim_lsp_status, cmp_nvim_lsp = pcall(require, "cmp_nvim_lsp")
if not cmp_nvim_lsp_status then
	print("auto complete not found")
	return
end

local keymap = vim.keymap

-- enable keybindes for buffer
local on_attach = function(client, bufnr)
	local opts = { noremap = true, silent = true, buffer = bufnr }

	--set keybinds
	keymap.set("n", "gf", "<cmd>Lspsaga lsp_finder<CR>", opts)
	keymap.set("n", "gD", "<Cmd>lua vim.lsp.buf.declaration()<CR>", opts)
	keymap.set("n", "gd", "<cmd>Lspsaga peek_definition<CR>", opts)
	keymap.set("n", "gi", "<cmd>lua vim.lsp.buf.implementation()<CR>", opts)
	keymap.set("n", "<leader>ca", "<cmd>Lspsaga code_action<CR>", opts)
	keymap.set("n", "<leader>rn", "<cmd>Lspsaga rename<CR>", opts)
	keymap.set("n", "<leader>d", "<cmd>Lspsaga show_line_diagnostics<CR>", opts)
	keymap.set("n", "<leader>d", "<cmd>Lspsaga show_cursor_diagnostics<CR>", opts)
	keymap.set("n", "[d", "<cmd>Lspsaga diagnostic_jump_prev<CR>", opts)
	keymap.set("n", "]d", "<cmd>Lspsaga diagnostic_jump_next<CR>", opts)
	keymap.set("n", "K", "<cmd>Lspsaga hover_doc<CR>", opts)
	keymap.set("n", "<leader>o", "<cmd>LSoutlineToggle<CR>", opts)
	keymap.set("n", "<leader>df", ":!dart format .<CR><CR>", opts)
end

-- used for autocompleteion
local capabilities = cmp_nvim_lsp.default_capabilities()

lspconfig["pyright"].setup({
	capabilities = capabilities,
	on_attach = on_attach,
	settings = {
		autoImportCompletion = true,
	},
	python = {
		analysis = {
			autoSearchPaths = true,
			diagnosticMode = "openFilesOnly",
			useLibraryCodeForTypes = true,
			typeCheckingMode = "off",
		},
	},
})

lspconfig["lua_ls"].setup({
	capabilities = capabilities,
	on_attach = on_attach,
	settings = { --custom settings for lua
		Lua = {
			--make lang server recognize vim
			diagnostics = {
				globals = { "vim" },
			},
			workspace = {
				-- make language server aware of runtime files
				library = {
					[vim.fn.expand("$VIMRUNTIME/lua")] = true,
					[vim.fn.stdpath("config") .. "/lua"] = true,
				},
			},
		},
	},
})
local util = require("lspconfig.util")

lspconfig["dartls"].setup({
	capabilities = capabilities,
	on_attach = on_attach,
	cmd = {
		"/home/aquil/softwares/flutter/bin/dart",
		"language-server",
		"--protocol=lsp",
	},
	init_options = {
		closingLabels = true,
		flutterOutline = true,
		onlyAnalyzeProjectsWithOpenFiles = true,
		outline = true,
		suggestFromUnimportedLibraries = true,
	},
	filetypes = { "dart" },
	settings = {
		autoImportCompletion = true,
	},
	root_dir = util.root_pattern("pubspec.yaml"),
	dart = {
		completeFunctionCalls = true,
		showTodos = true,
		analysis = {
			autoSearchPaths = true,
			diagnosticMode = "openFilesOnly",
			useLibraryCodeForTypes = true,
			typeCheckingMode = "off",
		},
		-- settings = {
		-- 	dart = {
		-- 		analysisExcludedFolders = {
		-- 			vim.fn.expand("/home/aquil/softwares/flutter/"),
		-- 		},
		-- 	},
		-- },
	},
})

-- require("flutter-tools").setup({
-- 	capabilities = capabilities,
-- 	on_attach = on_attach,
-- })

lspconfig["clangd"].setup({
	capabilities = capabilities,
	on_attach = on_attach,
	cmd = {
		"clangd",
		"--background-index",
		"--query-driver=/home/aquil/softwares/gcc-arm-none-eabi-10.3-2021.10/bin/arm-none-eabi-g++",
	},
	filetypes = { "c", "cpp", "objc", "objcpp" },
})
